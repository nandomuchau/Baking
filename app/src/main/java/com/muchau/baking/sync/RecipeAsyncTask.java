package com.muchau.baking.sync;

import android.content.Context;
import android.os.AsyncTask;

import com.muchau.baking.model.Recipe;
import com.muchau.baking.adapter.RecipeAdapter;
import com.muchau.baking.utilities.JsonUtils;
import com.muchau.baking.utilities.NetworkUtils;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Luis F. Muchau on 8/9/2018.
 */
public class RecipeAsyncTask extends AsyncTask<Void, Void, Void> {

    private Context context;

    private RecipeAdapter recipeAdapter;

    private ArrayList<Recipe> recipes;

    private static final String TAG = RecipeAsyncTask.class.getSimpleName();

    public RecipeAsyncTask(Context context, RecipeAdapter recipeAdapter){
        this.context = context;
        this.recipeAdapter = recipeAdapter;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        try {

            URL requestUrl = NetworkUtils.getUrl(this.context);
            String jsonResponse = NetworkUtils.getResponseFromHttpUrl(requestUrl);
            /* Parse the JSON into a list of recipes values */
            this.recipes = JsonUtils.getRecipesValuesFromJson(jsonResponse);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        recipeAdapter.updateRecipes(this.recipes);
        recipeAdapter.notifyDataSetChanged();
    }
}