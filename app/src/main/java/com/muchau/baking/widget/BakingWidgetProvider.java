package com.muchau.baking.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import com.muchau.baking.R;
import com.muchau.baking.ui.RecipeActivity;

import java.util.ArrayList;

import static com.muchau.baking.widget.BakingWidgetService.ACTIVITY_INGREDIENTS_LIST;

/**
 * Implementation of App Widget functionality.
 * Created by Luis F. Muchau on 8/21/2018.
 */
public class BakingWidgetProvider extends AppWidgetProvider {

    static ArrayList<String> ingredients = new ArrayList<>();

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {

        RemoteViews view = getRemoteView(context);

        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, view);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        // updateBakingWidgets(context, appWidgetManager, appWidgetIds);
    }

    public static void updateBakingWidgets(Context context, AppWidgetManager appWidgetManager,
                                           int[] appWidgetIds) {
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    private static RemoteViews getRemoteView(Context context) {

        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.baking_widget);

        // Call the activity when widget is clicked,
        // but resume activity from stack so you do not pass intent.extras
        Intent appIntent = new Intent(context, RecipeActivity.class);
        appIntent.addCategory(Intent.ACTION_MAIN);
        appIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        appIntent.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT|Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent appPendingIntent = PendingIntent
                .getActivity(context, 0, appIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        views.setPendingIntentTemplate(R.id.widget_grid_view, appPendingIntent);

        // Set the WidgetRemoteViewsService intent to act as the adapter for the GridView
        Intent intent = new Intent(context, WidgetRemoteViewsService.class);
        views.setRemoteAdapter(R.id.widget_grid_view, intent);

        return views;
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);

        int[] appWidgetIds = appWidgetManager.getAppWidgetIds(
                new ComponentName(context, BakingWidgetProvider.class));

        final String action = intent.getAction();

        if (action.equals(AppWidgetManager.ACTION_APPWIDGET_UPDATE)) {
            ingredients = intent.getExtras().getStringArrayList(ACTIVITY_INGREDIENTS_LIST);
            if (ingredients != null) {
                appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetIds, R.id.widget_grid_view);

                //Now update all widgets
                BakingWidgetProvider.updateBakingWidgets(context, appWidgetManager, appWidgetIds);
            }
        }
        super.onReceive(context, intent);
    }
}

