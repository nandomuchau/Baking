package com.muchau.baking.widget;

import android.app.IntentService;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;

import java.util.ArrayList;

/**
 * Created by Luis F. Muchau on 8/21/2018.
 */
public class BakingWidgetService extends IntentService {

    public static String ACTIVITY_INGREDIENTS_LIST = "activity_ingredients_list";

    private Context mContext;

    public BakingWidgetService() {
        super("BakingWidgetService");
    }

    public static void startBakingService(Context context, ArrayList<String> remoteIngredientsList) {
        Intent intent = new Intent(context, BakingWidgetService.class);
        intent.putExtra(ACTIVITY_INGREDIENTS_LIST, remoteIngredientsList);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            ArrayList<String> remoteIngredientsList = intent.getExtras()
                    .getStringArrayList(ACTIVITY_INGREDIENTS_LIST);
            handleActionUpdateBakingWidgets(remoteIngredientsList);
        }
    }

    private void handleActionUpdateBakingWidgets(ArrayList<String> remoteIngredientsList) {
        Intent intent = new Intent(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        intent.setComponent(new ComponentName(this, BakingWidgetProvider.class));
        intent.putExtra(ACTIVITY_INGREDIENTS_LIST, remoteIngredientsList);
        this.sendBroadcast(intent);
    }
}