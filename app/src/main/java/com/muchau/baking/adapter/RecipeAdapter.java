package com.muchau.baking.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.muchau.baking.ui.RecipeListActivity;
import com.muchau.baking.R;
import com.muchau.baking.model.Recipe;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Luis F. Muchau on 8/9/2018.
 */
public class RecipeAdapter extends RecyclerView.Adapter<RecipeAdapter.ViewHolder>{

    private ArrayList<Recipe> mData;

    private LayoutInflater mInflater;

    private Context mContext;

    // data is passed into the constructor
    public RecipeAdapter(Context context, ArrayList<Recipe> data) {
        this.mContext = context;
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
    }

    // inflates the cell layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.content_item, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the textView in each cell
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Recipe recipe = mData.get(position);
        holder.recipeTextView.setText(recipe.getName());
    }

    // total number of cells
    @Override
    public int getItemCount() {
        return mData!=null ? mData.size() : 0;
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.recipe_text_view) TextView recipeTextView;
        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
           Recipe recipe = getItem(getAdapterPosition());
            if(mContext instanceof RecipeListActivity){
                ((RecipeListActivity)mContext).loadRecipeActivity(recipe);
            }
        }
    }

    public void updateRecipes(ArrayList<Recipe> recipes) {
        this.mData = recipes;
    }

    private Recipe getItem(int id) {
        return mData!= null ? mData.get(id) : null;
    }
}
