package com.muchau.baking.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.muchau.baking.R;
import com.muchau.baking.model.Step;
import com.muchau.baking.ui.RecipeActivity;
import com.muchau.baking.ui.StepDetailActivity;
import com.muchau.baking.ui.StepDetailFragment;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Luis F. Muchau on 8/12/2018.
 */
public class StepAdapter extends RecyclerView.Adapter<StepAdapter.ViewHolder> {

    private final RecipeActivity mParentActivity;

    private final ArrayList<Step> mSteps;

    private final boolean mTwoPane;

    public StepAdapter(RecipeActivity parent, ArrayList<Step> steps, boolean twoPane) {
        mSteps = steps;
        mParentActivity = parent;
        mTwoPane = twoPane;
    }

    private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Step step = (Step) view.getTag();
            if (mTwoPane) {
                useStepDetailFragment(step.getId());
            } else {
                    Context context = view.getContext();
                    Intent intent = new Intent(context, StepDetailActivity.class);
                    intent.putExtra(StepDetailFragment.ARG_STEP_ID, step.getId());
                    intent.putParcelableArrayListExtra(StepDetailFragment.ARG_STEPS, mSteps);

                    context.startActivity(intent);
            }
        }
    };

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.content_step, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.stepTextView.setText(mSteps.get(position).getShortDescription());

        holder.itemView.setTag(mSteps.get(position));
        holder.itemView.setOnClickListener(mOnClickListener);

        if(position == 0 && mTwoPane) {
            holder.itemView.setSelected(true);
            useStepDetailFragment(position);
        }
    }

    @Override
    public int getItemCount() {
        return mSteps.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.steps_text_view) TextView stepTextView;
        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    private void useStepDetailFragment(int step_id) {
        Bundle arguments = new Bundle();
        arguments.putInt(StepDetailFragment.ARG_STEP_ID, step_id);
        arguments.putParcelableArrayList(StepDetailFragment.ARG_STEPS, mSteps);
        StepDetailFragment fragment = new StepDetailFragment();
        fragment.setArguments(arguments);
        mParentActivity.getSupportFragmentManager().beginTransaction()
                .replace(R.id.step_detail_container, fragment)
                .commit();
    }
}
