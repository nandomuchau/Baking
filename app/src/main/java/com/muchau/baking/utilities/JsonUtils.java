package com.muchau.baking.utilities;

import com.muchau.baking.model.Ingredient;
import com.muchau.baking.model.Recipe;
import com.muchau.baking.model.Step;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Luis F. Muchau on 5/9/2018.
 */
public class JsonUtils {

    // Recipes list
    private static final String ID = "id";
    private static final String NAME = "name";
    private static final String INGREDIENTS = "ingredients";
    private static final String STEPS = "steps";
    private static final String SERVINGS = "servings";
    private static final String IMAGE = "image";

    // Ingredients List
    private static final String INGREDIENT = "ingredient";
    private static final String QUANTITY = "quantity";
    private static final String MEASURE = "measure";

    // Steps List
    private static final String SHORT_DESCRIPTION = "shortDescription";
    private static final String DESCRIPTION = "description";
    private static final String VIDEO_URL = "videoURL";
    private static final String THUMBNAIL_URL = "thumbnailURL";

    public static ArrayList<Recipe> getRecipesValuesFromJson(String mainJsonStr)
            throws JSONException {

        ArrayList<Recipe> list = new ArrayList<>();
        ArrayList<Ingredient> ingredientList;
        ArrayList<Step> stepList;
        Recipe recipe;
        Ingredient ingredient;
        Step step;

        JSONArray jsonArray = new JSONArray(mainJsonStr);

        for (int i = 0; i < jsonArray.length(); i++) {
            recipe = new Recipe();

            JSONObject obj = jsonArray.getJSONObject(i);

            recipe.setId(obj.getInt(ID));
            recipe.setName(obj.getString(NAME));
            recipe.setServings(obj.getInt(SERVINGS));
            recipe.setImage(obj.getString(IMAGE));

            ingredientList = new ArrayList<>();
            JSONArray ingredientsArray = obj.getJSONArray(INGREDIENTS);
            for (int j = 0; j < ingredientsArray.length(); j++) {
                ingredient = new Ingredient();
                JSONObject ingredientObj = ingredientsArray.getJSONObject(j);

                ingredient.setQuantity(ingredientObj.getInt(QUANTITY));
                ingredient.setMeasure(ingredientObj.getString(MEASURE));
                ingredient.setIngredient(ingredientObj.getString(INGREDIENT));

                ingredientList.add(ingredient);
            }
            recipe.setIngredients(ingredientList);

            stepList = new ArrayList<>();
            JSONArray stepsArray = obj.getJSONArray(STEPS);
            for (int k = 0; k < stepsArray.length(); k++) {
                step = new Step();
                JSONObject stepObj = stepsArray.getJSONObject(k);

                step.setId(stepObj.getInt(ID));
                step.setShortDescription(stepObj.getString(SHORT_DESCRIPTION));
                step.setDescription(stepObj.getString(DESCRIPTION));
                step.setVideoURL(stepObj.getString(VIDEO_URL));
                step.setThumbnailURL(stepObj.getString(THUMBNAIL_URL));

                stepList.add(step);
            }
            recipe.setSteps(stepList);
            list.add(recipe);
        }

        return list;
    }
}
