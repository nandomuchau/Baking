package com.muchau.baking.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.widget.Toast;

import com.muchau.baking.R;
import com.muchau.baking.adapter.RecipeAdapter;
import com.muchau.baking.model.Recipe;
import com.muchau.baking.sync.RecipeAsyncTask;
import com.muchau.baking.utilities.NetworkUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Luis F. Muchau on 8/9/2018.
 */
public class RecipeListActivity extends AppCompatActivity {

    private static final String TAG = RecipeListActivity.class.getSimpleName();

    private static final String KEY_INSTANCE_STATE_RV_POSITION = "grid_position";

    private RecipeAdapter recipeAdapter;

    private Parcelable savedRecyclerLayoutState;

    @BindView(R.id.recycler_view_recipes)
    RecyclerView recyclerView;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_list);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        int numberOfColumns = calculateNoOfColumns(this);
        recyclerView.setLayoutManager(new GridLayoutManager(this, numberOfColumns));
        recipeAdapter = new RecipeAdapter(this, new ArrayList<Recipe>());
        recyclerView.setAdapter(recipeAdapter);
        loadRecipes();
    }

    private void loadRecipes(){
        if (NetworkUtils.isInternetAvailable(this)) {
            new RecipeAsyncTask(this, recipeAdapter) {
                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    if (savedRecyclerLayoutState != null) {
                        recyclerView.getLayoutManager()
                                .onRestoreInstanceState(savedRecyclerLayoutState);
                    }
                }
            }.execute();
        } else {
            Toast.makeText(this, R.string.no_internet, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        //restore recycler view at same position
        if (savedInstanceState != null) {
            savedRecyclerLayoutState =
                    savedInstanceState.getParcelable(KEY_INSTANCE_STATE_RV_POSITION);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(KEY_INSTANCE_STATE_RV_POSITION,
                recyclerView.getLayoutManager().onSaveInstanceState());
        super.onSaveInstanceState(outState);
    }

    private static int calculateNoOfColumns(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
        int scalingFactor = 300;
        int noOfColumns = (int) (dpWidth / scalingFactor);
        if(noOfColumns < 1)
            noOfColumns = 1;
        return noOfColumns;
    }

    public void loadRecipeActivity(Recipe recipe) {
        Intent intent = new Intent(this, RecipeActivity.class);
        intent.putExtra("recipe", recipe);
        intent.putParcelableArrayListExtra("ingredients", recipe.getIngredients());
        intent.putParcelableArrayListExtra("steps", recipe.getSteps());
        startActivity(intent);
    }
}
