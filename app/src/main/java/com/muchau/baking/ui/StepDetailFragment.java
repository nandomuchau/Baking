package com.muchau.baking.ui;

import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.Renderer;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveVideoTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlaybackControlView;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.Allocator;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.muchau.baking.R;
import com.muchau.baking.model.Step;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Optional;
import butterknife.Unbinder;

/**
 * Created by Luis F. Muchau on 8/18/2018.
 */
public class StepDetailFragment extends Fragment {

    public static final String ARG_STEP_ID = "step_id";

    public static final String ARG_STEPS = "steps";

    public ArrayList<Step> steps;

    private Step mStep;

    private Unbinder unbinder;

    @Nullable
    @BindView(R.id.step_detail)
    TextView mStepDetail;

    @Nullable
    @BindView(R.id.step_image_view)
    ImageView mStepImage;

    @BindView(R.id.exoPlayerView)
    SimpleExoPlayerView mPlayerView;

    private SimpleExoPlayer mPlayer;

    public StepDetailFragment() {
    }

    private String videoUrl;
    private long exo_current_position = 0;
    private boolean playerStopped = false;
    private long playerStopPosition;

    private static final String EXO_CURRENT_POSITION = "current_position";
    private static final String PLAYER_STOPPED = "player_stopped";
    private static final String EXO_STOP_POSITION = "stop_position";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARG_STEP_ID)) {
            steps = getArguments().getParcelableArrayList(ARG_STEPS);
            mStep = steps.get(getArguments().getInt(ARG_STEP_ID));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.step_detail, container, false);
        unbinder = ButterKnife.bind(this, rootView);

        if (savedInstanceState != null) {
            exo_current_position = savedInstanceState.getLong(EXO_CURRENT_POSITION);
            playerStopped = savedInstanceState.getBoolean(PLAYER_STOPPED);
            playerStopPosition = savedInstanceState.getLong(EXO_STOP_POSITION);
        }

        if (mStep != null) {
            if (mStepDetail != null) {
                mStepDetail.setText(mStep.getDescription());
            }
            if (mStepImage != null) {
                Picasso.get()
                        .load(mStep.getThumbnailURL())
                        .placeholder(R.drawable.ic_img_placeholder)
                        .error(R.drawable.ic_img_placeholder)
                        .into(mStepImage);
            }
            videoUrl = mStep.getVideoURL();

            if (videoUrl != "") {
                if (!mStep.getVideoURL().equals("")) {
                    initializePlayer(Uri.parse(mStep.getVideoURL()));
                    getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);
                } else {
                    assert mPlayerView != null;
                    mPlayerView.setVisibility(View.GONE);
                }
            }
        }
        return rootView;
    }

    private void initializePlayer(Uri mediaUri) {
        if (mPlayer == null) {
            TrackSelector trackSelector = new DefaultTrackSelector();
            LoadControl loadControl = new DefaultLoadControl();

            mPlayer = ExoPlayerFactory.newSimpleInstance(getContext(), trackSelector, loadControl);
            mPlayerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);
            mPlayerView.setPlayer(mPlayer);

            String userAgent = Util.getUserAgent(getContext(), "Baking");
            MediaSource mediaSource = new ExtractorMediaSource(mediaUri, new DefaultDataSourceFactory(
                    getContext(), userAgent), new DefaultExtractorsFactory(), null, null);

            mPlayer.addListener(new SimpleExoPlayer.EventListener() {

                @Override
                public void onTimelineChanged(Timeline timeline, Object manifest) {

                }

                @Override
                public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

                }

                @Override
                public void onLoadingChanged(boolean isLoading) {

                }

                @Override
                public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                    playerStopped = !playWhenReady;
                }

                @Override
                public void onPlayerError(ExoPlaybackException error) {

                }

                @Override
                public void onPositionDiscontinuity() {

                }
            });

            mPlayer.setPlayWhenReady(!playerStopped);
            if (exo_current_position != 0 && !playerStopped) {
                mPlayer.seekTo(exo_current_position);
            } else {
                mPlayer.seekTo(playerStopPosition);
            }
            mPlayer.prepare(mediaSource);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mPlayer != null) {
            outState.putLong(EXO_CURRENT_POSITION, mPlayer.getCurrentPosition());
        }
        outState.putBoolean(PLAYER_STOPPED, playerStopped);
        outState.putLong(EXO_STOP_POSITION, playerStopPosition);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (Util.SDK_INT > 23) {
            initializePlayer(Uri.parse(videoUrl));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if ((Util.SDK_INT <= 23 || mPlayer == null)) {
            initializePlayer(Uri.parse(videoUrl));
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mPlayer != null) {
            playerStopPosition = mPlayer.getCurrentPosition();
            releasePlayer();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mPlayer != null) {
            playerStopPosition = mPlayer.getCurrentPosition();
            //releasePlayer();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        releasePlayer();
    }

    /**
     * Method to release exoPlayer
     */
    private void releasePlayer() {
        if (mPlayer != null) {
            mPlayer.stop();
            mPlayer.release();
            mPlayer = null;
        }
    }
}
