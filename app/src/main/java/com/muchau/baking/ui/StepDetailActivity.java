package com.muchau.baking.ui;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.muchau.baking.R;
import com.muchau.baking.model.Step;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Luis F. Muchau on 8/18/2018.
 */
public class StepDetailActivity extends AppCompatActivity {

    ArrayList<Step> mSteps;

    int step_id;

    int page_id;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.next_button)
    Button mNextButton;

    @BindView(R.id.previous_button)
    Button mPreviousButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step_detail);
        ButterKnife.bind(this);

        if (savedInstanceState == null) {
            // Create the detail fragment and add it to the activity
            // using a fragment transaction.
            Bundle arguments = new Bundle();

            step_id = getIntent().getIntExtra(StepDetailFragment.ARG_STEP_ID,0);
            arguments.putInt(StepDetailFragment.ARG_STEP_ID, step_id);
            mSteps = getIntent().getParcelableArrayListExtra(StepDetailFragment.ARG_STEPS);
            arguments.putParcelableArrayList(StepDetailFragment.ARG_STEPS, mSteps);

            StepDetailFragment fragment = new StepDetailFragment();
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.step_detail_container, fragment)
                    .commit();

            Step step = mSteps.get(step_id);
            toolbar.setTitle(step.getShortDescription());

            page_id = step_id;
        }
        setSupportActionBar(toolbar);
        // Show the Up button in the action bar.
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        int orientation = getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            hideOrShowNavigation("landscape");
        } else {
            hideOrShowNavigation("");
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
           onBackPressed();
           return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void hideOrShowNavigation(String view) {
        if (view != "landscape") {
            mPreviousButton.setVisibility(View.VISIBLE);
            mNextButton.setVisibility(View.VISIBLE);
            if (page_id == 0) {
                mPreviousButton.setVisibility(View.GONE);
            } else if (page_id == mSteps.size() - 1) {
                mNextButton.setVisibility(View.GONE);
            }
        } else {
            mPreviousButton.setVisibility(View.GONE);
            mNextButton.setVisibility(View.GONE);
        }
    }

    public void previousStep(View view) {
        page_id = ((page_id - 1) <= 0) ? 0 : (page_id - 1);
        changeStepView(page_id);
        hideOrShowNavigation("");
    }

    public void nextStep(View view) {
        if (mSteps != null) {
            page_id = ((page_id + 1) >= mSteps.size()) ? mSteps.size() - 1 : (page_id + 1);
            changeStepView(page_id);
        }
        hideOrShowNavigation("");
    }

    private void changeStepView(int stepId) {
        if (mSteps != null) {
            Bundle arguments = new Bundle();
            arguments.putInt(StepDetailFragment.ARG_STEP_ID, stepId);
            arguments.putParcelableArrayList(StepDetailFragment.ARG_STEPS, mSteps);
            StepDetailFragment fragment = new StepDetailFragment();
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.step_detail_container, fragment)
                    .commit();

            Step step = mSteps.get(stepId);
            toolbar.setTitle(step.getShortDescription());
        }
    }
}
