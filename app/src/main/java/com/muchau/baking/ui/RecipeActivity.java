package com.muchau.baking.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.muchau.baking.R;
import com.muchau.baking.adapter.StepAdapter;
import com.muchau.baking.model.Ingredient;
import com.muchau.baking.model.Recipe;
import com.muchau.baking.model.Step;
import com.muchau.baking.widget.BakingWidgetService;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Luis F. Muchau on 8/15/2018.
 */
public class RecipeActivity extends AppCompatActivity {

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet device.
     */
    private boolean mTwoPane;

    private Recipe recipe;

    private ArrayList<Ingredient> ingredients;

    public ArrayList<Step> steps;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.ingredients_text_view)
    TextView ingredients_text_view;

    @BindView(R.id.recycler_view_steps)
    View recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe);
        ButterKnife.bind(this);

        Bundle data = getIntent().getExtras();
        recipe = data.getParcelable("recipe");
        ingredients = this.getIntent().getParcelableArrayListExtra("ingredients");
        steps = this.getIntent().getParcelableArrayListExtra("steps");

        toolbar.setTitle(recipe.getName());
        setSupportActionBar(toolbar);

        updateWidgetData();

        // Show the Up button in the action bar.
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        ingredients_text_view.setText(loadIngredients());

        if (findViewById(R.id.step_detail_container) != null) {
            // The detail container view will be present only in the large-screen layouts
            //(res/values-w900dp). If this view is present, the activity should be in two-pane mode.
            mTwoPane = true;
        }

        assert recyclerView != null;
        setupRecyclerView((RecyclerView) recyclerView);
    }

    private void updateWidgetData() {
        ArrayList<String> ingredientsWidget= new ArrayList<>();
        for (Ingredient ing : ingredients) {
            ingredientsWidget.add(ing.toString());
        }
        BakingWidgetService.startBakingService(this, ingredientsWidget);
    }

    private void setupRecyclerView(@NonNull RecyclerView recyclerView) {
        recyclerView.setAdapter(new StepAdapter(this, steps, mTwoPane));
    }

    private StringBuilder loadIngredients() {
        StringBuilder stringBuilder = new StringBuilder();

        for (Ingredient ing : ingredients) {
            stringBuilder.append(ing.toString());
            stringBuilder.append("\n");
        }
        if (stringBuilder.length() > 0) {
            stringBuilder.deleteCharAt(stringBuilder.length()-1);
        }
        return stringBuilder;
    }
}
