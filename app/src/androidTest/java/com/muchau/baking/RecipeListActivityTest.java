package com.muchau.baking;

import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.muchau.baking.ui.RecipeListActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withEffectiveVisibility;
import static org.hamcrest.Matchers.allOf;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static android.support.test.espresso.matcher.ViewMatchers.withClassName;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.endsWith;

/**
 * Created by Luis F. Muchau on 8/25/2018.
 */
@RunWith(AndroidJUnit4.class)
public class RecipeListActivityTest {

    @Rule
    public ActivityTestRule<RecipeListActivity> mActivityTestRule = new ActivityTestRule<>(RecipeListActivity.class);

    @Test
    public void testFindRecipeInTheList() {
        onView(withId(R.id.recycler_view_recipes))
                .check(matches(hasDescendant(withText("Brownies"))));
    }

    @Test
    public void testRecipeClickAndFindFirstStepInTheList() {
        onView(withId(R.id.recycler_view_recipes))
                .perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));

        onView(withId(R.id.recycler_view_steps))
                .check(matches(hasDescendant(withText("Recipe Introduction"))));
    }

    @Test
    public void testStepClick() {
        onView(withId(R.id.recycler_view_recipes))
                .perform(RecyclerViewActions.actionOnItemAtPosition(1, click()));

        onView(withId(R.id.recycler_view_steps))
                .perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));

        onView(allOf(
                anyOf(
                        withId(R.id.exoPlayerView)
                ),
                withClassName(endsWith(SimpleExoPlayerView.class.getName())),
                withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)))
                .check(matches(isDisplayed()));

    }
}
